# import re
import ujson
import user_agents

def agents(request):
    if request.headers.get('User-Agent'):
        p=user_agents.parse(request.headers['User-Agent'])
        result={
            'browser_name':p.browser.family,
            'browser_version':p.browser.version_string,
            'device_brand':p.device.brand,
            'device_name':p.device.family,
            'device_model':p.device.model,
            'os_name':p.os.family,
            'os_version':p.os.version_string,
            'is_mobile':p.is_mobile,
            'is_pc':p.is_pc,
            'is_tablet':p.is_tablet,
            'is_bot':p.is_bot,
            'is_touch_capable':p.is_touch_capable,
        }
    else:
        result={}
    result['full_url']=request.full_url()
    result['method']=request.method
    result['request_time']=request.request_time()
    result['protocol']=request.protocol
    result['remote_ip']=request.remote_ip
    result['path']=request.path
    result['arguments']=ujson.dumps(request.arguments)
    return result

def is_mobile(request):
    """
    """
    userAgent = agents(request)
    return userAgent['is_mobile']
