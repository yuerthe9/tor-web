使用说明
===
    app = WebApplication(debug=True,login_url='/',file_extension='html',file_encoding='utf-8')

    1、登录校验：
        前提——需要配置login_url参数：app = WebApplication(login_url='/')。
                需要重写get_current_user，返回当前登录用户信息。
        默认——开启状态，对于登录、退出、首页等无需校验的url需要手动关闭检验：auth=False
        运行时设置——无
        样例：
        @app.route('/login',auth=False)
        class SayHandler(MainHandler):
            def get(self):
                self.render_text('<h1>main - {{template}}</h1>',{'template':txt})
    2、角色控制：
        前提——重写get_current_role，返回登录用户角色名称。
        默认——不进行控制，默认值是空列表。
        运行时设置——重写MainHandler.refresh_role(self)，将动态的角色控制逻辑整理好，该方法将在运行时替换
            装饰器的参数方式，也就是说如果重写了refresh_role那么优先进行处理。
        样例：
        @app.route('/login',role=['admin'])
        class SayHandler(MainHandler):
            def get(self):
                self.render_text('<h1>main - {{template}}</h1>',{'template':txt})
    3、方法控制：
        前提——无
        默认——包含POST,GET
        运行时设置——无
        样例：
        @app.route('/login',method=['GET'])
        class SayHandler(MainHandler):
            def get(self):
                self.render_text('<h1>main - {{template}}</h1>',{'template':txt})
    4、日志控制：
        前提——需要准备一个函数，参数有2个，第一个为handler的名称；第二个是该handler当时的request对象。
        默认——None，不进行日志记录
        运行时设置——无
        样例：
        @app.route('/login',log_func=log_to_mongodb)
        class SayHandler(MainHandler):
            def get(self):
                self.render_text('<h1>main - {{template}}</h1>',{'template':txt})
    5、移动控制：
        前提：无
        默认：不区分pc/手机版，当设置此值后，如果监测到客户端是手机版将重定向到指定mobile_url处。
        运行时设置：无
        样例：
        @app.route('/login',mobile_url='/mobile_login')
        class SayHandler(MainHandler):
            def get(self):
                self.render_text('<h1>main - {{template}}</h1>',{'template':txt})
    6、token控制：
        前提：参数中必须要有"token"参数
        默认：不进行控制。
        运行时设置：重写MainHandler.refresh_tokens(self)，要求返回一个包含有效token的list。
        样例：
        @app.route('/login',token=True)
        class SayHandler(MainHandler):
            def refresh_tokens(self):
                return ['1','2']
            def get(self):
                self.render_text('<h1>main - {{template}}</h1>',{'template':txt})

版本说明
===

|版本号|说明|
|---|---|
|0.0.6.dev1|恢复send_error，同时提供before_exec/after_exec和before_render来帮助参与框架运行。避免通过重写来修改框架代码。|
|0.0.5.dev2|将send_error替换为raise HTTPError来中断请求|
|0.0.5.dev1|增加token控制|
|0.0.4.1.dev1|修正methods大小写问题|
|0.0.4.dev1|增加缓存|
|0.0.3.dev1|增加移动版本检查|
|0.0.2.dev1|增加命令行关闭回调|
|0.0.1.dev1|基础版本|