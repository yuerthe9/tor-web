#encoding:utf-8
#!/usr/bin/env python
from setuptools import setup

setup(
    name='tor-web',
    version='0.1.10.dev1',
    description=u'基于tornado web改良的web框架，在异步的世界里让web更贴近实战、更好用。',
    url='https://gitee.com/yuerthe9/tor-web',
    author=u'公子玉',
    author_email='yuerthe9@aliyun.com',
    license='MIT',
    #https://pypi.org/pypi?%3Aaction=list_classifiers
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'Topic :: Software Development :: Build Tools',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 3',
    ],
    keywords='tornado web async',
    packages='.',#source code dir
)
