from torweb import tornado,WebApplication,MainHandler,executor_g
from tornado.queues import Queue
from tornado import gen
import asyncio
import time
from pprint import pprint
from torutil import agents
# from asyncio import Queue

if __name__ == "__main__":
    app = WebApplication(cookie_secret='dddddd',xheaders=True,debug=True,login_url='/',file_extension='html',file_encoding='utf-8')

    @app.route('/',role=[],auth=False,method=['POST','GET'],ip=[],log_func=None,name='中国',code='index')
    class SayHandler(MainHandler):
        def get(self):
            txt=self.get_argument('t','Say...')
            print(self.get_cookie_value('userid','yuer'))
            # print(dir(self.request))
            result=agents(self.request)
            result=[{'key':k,'value':v} for k,v in result.items()]
            pprint(result)
            self.render_text('<h1>{{_menus}}kkkkk - {{template}}</h1>{{ip}}<br><form action="/" method="post"><input name="name" value="ada"><button type="submit">s</button></form><br><a href="/?a=ada">s</a><hr><table border=1><tr><th>Key</th><th>Value</th></tr>{{#table}}<tr><td align="right">{{key}}</td><td>{{value}}</td></tr>{{/table}}</table>',{'template':self.request,'ip':self.request.remote_ip,'table':result})
        def post(self):
            self.get()
        def refresh_role(self):
            return []

    @app.route('/hi',role=[],auth=False,method=['POST','GET'],log_func=None)
    class HelloHandler(MainHandler):
        def get(self):
            txt=self.get_argument('t','Hello...')
            self.render_text('<h1>main - {{template}}</h1>',{'template':txt})
        def refresh_role(self):
            return []

    q = Queue()

    def consumer(q):
        try:
            if not q.empty():
                for _ in range(5):
                    if q.empty():
                        break
                    item = q.get_nowait()
                    print('====> %s' % item)
                time.sleep(5)
                print('finish')
            else:
                print('nothing')
        except Exception as e:
            print(e)
        finally:
            # try:
            #     q.task_done()
            # except Exception as j:
            #     print(j)
            pass

    @app.route('/push',role=[],auth=False,method=['POST','GET'],log_func=None)
    class HelloHandler(MainHandler):
        
        async def get(self):
            txt=self.get_argument('t',None)
            if txt:
                await q.put(txt)
                self.render_text('<h1>{{template}} - {{size}} - Empty:{{empty}}</h1>',{'template':txt,'size':q.qsize(),'empty':q.empty()})
            else:
                self.render_text('<h1>Nothing</h1>')
            if not q.empty():
                executor_g.submit(consumer,q)
            # await consumer()

    print(tornado.version)
    app.listen(9527)
    tornado.ioloop.IOLoop.current().start()